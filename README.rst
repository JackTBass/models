******************
Analytics & Models
******************

In this repository we will see examples about:

- Machine learning models
- Analytical techniques & data preprocessing
- Statistic analysis
- Graphical analysis

Machine Learning Models
***********************

In this section we will see how to implement simple versions of machine
learning models for regression and classification problems, also,
in these algorithms we will find examples of supervised learning and
unsupervised learning.

- Classifications models
    - Deep Neural Network (Classification) | Supervised & Unsupervised
    - K-Means | Unsupervised
    - K-Nearest-Neighbors (KNN) | Unsupervised
    - Naïve Bayes Algorithms | Supervised
    - Logistic Regression | Supervised
    - Support Vector Machines (SMV) | Supervised
    - Trees Algorithms (Decision Tree & Random Forest) | Supervised

- Regression models
    - Deep Neural Network (Regression) | Supervised & Unsupervised
    - Linear regression | Supervised
    - Polynomial regression | Supervised
    - Trees Algorithms (Decision Tree & Random Forest) | Supervised

Analytical techniques & Data preprocessing
******************************************

In this section we will see techniques that allow us to obtain information
about the data and we will also see how to transform the data to represent
and process it in a more analytical way.

We will see preprocessing techniques such as:

- Encode Dictionary
(To be continue)